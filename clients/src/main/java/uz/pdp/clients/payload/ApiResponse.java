package uz.pdp.clients.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cloud.openfeign.FeignClient;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/23/22 9:44 PM


public class ApiResponse {
    private String message;
    private boolean status;
    private Object data;
}
