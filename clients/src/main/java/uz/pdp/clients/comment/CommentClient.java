package uz.pdp.clients.comment;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import uz.pdp.clients.dto.CommentDto;

import java.util.List;

@FeignClient("book-review-service")
public interface CommentClient {
    @GetMapping("/api/comment/with-users/{bookId}")
    List<CommentDto> getAllCommentsWithUsersByBookId(@PathVariable("bookId") Long bookId);

}
