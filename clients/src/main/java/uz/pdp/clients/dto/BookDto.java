package uz.pdp.clients.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/25/22 4:09 PM

public class BookDto {

    private Long id;

    private String title;

    private Double price;

    private String description;
    private List<UserDto> authors = new ArrayList<>();
    private List<CommentDto> comments = new ArrayList<>();

}
