package uz.pdp.clients.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/25/22 3:55 PM

public class CommentDto {
    private Long id;
    private String description;
    private Timestamp creationDate;
    private Long userId;
    private String userFullName;
    private Long userProfileImageId;
}
