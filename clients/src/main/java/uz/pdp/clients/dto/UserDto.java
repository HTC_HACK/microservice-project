package uz.pdp.clients.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/25/22 4:11 PM

public class UserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
}
