package uz.pdp.clients.projection;

public interface AuthorProjection {

    Long getId();
    String getFirstName();
    String getLastName();


}
