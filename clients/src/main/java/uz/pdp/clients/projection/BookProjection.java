package uz.pdp.clients.projection;

import org.springframework.beans.factory.annotation.Value;

import java.util.List;


public interface BookProjection {

    Long getId();

    String getTitle();

    Double getPrice();

    String getDescription();

    @Value("#{@authorRepo.getAllAuthors(target.id)}")
    List<AuthorProjection> getAuthors();

}
