package uz.pdp.clients.projection;

import java.sql.Timestamp;

public interface CommentProjection {

    Long getId();
    String getDescription();
    Timestamp getCreationDate();
    Long getUserId();
    String getUserFullName();
    Long getUserProfileImage();

}
