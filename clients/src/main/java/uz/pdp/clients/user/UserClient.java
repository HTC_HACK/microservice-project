package uz.pdp.clients.user;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import uz.pdp.clients.dto.UserDto;

@FeignClient("user-service")
public interface UserClient {

    @GetMapping("/api/user/{userId}")
    UserDto getUserById(@PathVariable("userId") Long userId);

}
