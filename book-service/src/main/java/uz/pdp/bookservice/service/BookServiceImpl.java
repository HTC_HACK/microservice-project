package uz.pdp.bookservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.pdp.bookservice.repo.AuthorRepo;
import uz.pdp.bookservice.repo.BookRepo;
import uz.pdp.clients.comment.CommentClient;
import uz.pdp.clients.dto.BookDto;
import uz.pdp.clients.dto.CommentDto;
import uz.pdp.clients.dto.UserDto;
import uz.pdp.clients.payload.ApiResponse;
import uz.pdp.clients.projection.BookProjection;
import uz.pdp.clients.user.UserClient;

import java.util.ArrayList;
import java.util.List;


//Asatbek Xalimojnov 4/23/22 9:36 PM

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepo bookRepo;
    private final AuthorRepo authorRepo;
    private final RestTemplate restTemplate;
    private final CommentClient commentClient;
    private final UserClient userClient;


    @Override
    public ResponseEntity getAllBooks(int page, int size) {

        Pageable pageable = PageRequest.of(page - 1, size);

        return new ResponseEntity<>(
                new ApiResponse("success",
                        true,
                        bookRepo.getAllBooks(pageable)),
                HttpStatus.OK);
    }

    @Override
    public ResponseEntity getBookById(Long bookId) {

        BookProjection book = bookRepo.getBookById(bookId);

        List<UserDto> authors = new ArrayList<>();
        book.getAuthors().stream()
                .forEach(authorProjection -> authors.add(new UserDto(authorProjection.getId(), authorProjection.getFirstName(), authorProjection.getLastName(),null)));

        List<CommentDto> comments = new ArrayList<>();
        for (CommentDto commentDto : commentClient.getAllCommentsWithUsersByBookId(bookId)) {
            UserDto user = userClient.getUserById(commentDto.getUserId());
            if (user!=null) {
                commentDto.setUserFullName(user.getFirstName() + " " + user.getLastName());
            }
            comments.add(commentDto);
        }

        BookDto bookDto = new BookDto(
                book.getId(),
                book.getTitle(),
                book.getPrice(),
                book.getDescription(),
                authors,
                comments);

        return new ResponseEntity<>(
        new ApiResponse(
                "success",
                true,
                bookDto),
                HttpStatus.OK);
    }

}
