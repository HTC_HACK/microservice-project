package uz.pdp.bookservice.service;

import org.springframework.http.ResponseEntity;

public interface BookService  {

    ResponseEntity getAllBooks(int page, int size);

    ResponseEntity getBookById(Long bookId);





}
