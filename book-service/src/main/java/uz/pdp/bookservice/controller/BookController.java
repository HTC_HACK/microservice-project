package uz.pdp.bookservice.controller;



//Asatbek Xalimojnov 4/23/22 9:41 PM

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bookservice.service.BookServiceImpl;
import uz.pdp.clients.payload.ApiResponse;

@RestController
@RequestMapping("/api/book")
@RequiredArgsConstructor
public class BookController {


    private final BookServiceImpl bookService;
    final String DEFAULT_PAGE="1";
    final String DEFAULT_PAGE_SIZE="3";


    @GetMapping
    public ResponseEntity<ApiResponse> getAllBooksWithAuthors(@RequestParam(required = false,defaultValue = DEFAULT_PAGE)int page,
                                   @RequestParam(required = false,defaultValue = DEFAULT_PAGE_SIZE)int size) {
       return bookService.getAllBooks(page,size);

    }

    @GetMapping("/{bookId}")
    public ResponseEntity<ApiResponse> getBookById(@PathVariable Long bookId) {
        return bookService.getBookById(bookId);
    }

}
