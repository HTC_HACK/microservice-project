package uz.pdp.bookservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bookservice.model.Author;
import uz.pdp.clients.projection.AuthorProjection;

import java.util.List;

public interface AuthorRepo extends JpaRepository<Author, Long> {


    @Query(nativeQuery = true,
            value = "select id,first_name as firstName,last_name as lastName\n" +
                    "from authors\n" +
                    "join book_authors ba on authors.id = ba.author_id\n" +
                    "where ba.book_id=?1")
    List<AuthorProjection> getAllAuthors(Long id);
}
