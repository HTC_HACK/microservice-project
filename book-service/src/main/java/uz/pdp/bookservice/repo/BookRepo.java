package uz.pdp.bookservice.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bookservice.model.Book;
import uz.pdp.clients.projection.BookProjection;

public interface BookRepo extends JpaRepository<Book,Long> {

    @Query(nativeQuery = true,
    value = "select books.id                                                                                          as id,\n" +
            "       title,\n" +
            "       price,\n" +
            "       description\n" +
            "from books")
    Page<BookProjection> getAllBooks(Pageable pageable);

    @Query(nativeQuery = true,
            value = "select books.id                                                                                          as id,\n" +
                    "       title,\n" +
                    "       price,\n" +
                    "       description\n" +
                    "from books where id=?1")
    BookProjection getBookById(Long bookId);

}
