package uz.pdp.consumermessagebroker.service;


//Asatbek Xalimojnov 4/23/22 5:10 PM


import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import uz.pdp.clients.dto.UserDto;

import java.time.LocalDateTime;

@Service
public class ConsumerService {


    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public void receiveMessages(UserDto customerDto)
    {
        //***
        //logic code goes here
        //**

        System.out.println(LocalDateTime.now() + " : " + customerDto.toString());

    }

}
