package uz.pdp.userservice.service;


//Asatbek Xalimojnov 4/25/22 4:41 PM


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.clients.dto.UserDto;
import uz.pdp.userservice.model.User;
import uz.pdp.userservice.repo.UserRepo;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;


    @Override
    public UserDto getUserById(Long id) {
        if (userRepo.findById(id).isPresent()) {
            User user = userRepo.findById(id).get();
            return new UserDto(user.getId(), user.getFirstName(), user.getLastName(), user.getEmail());
        }
        return null;
    }
}
