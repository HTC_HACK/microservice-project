package uz.pdp.userservice.service;

//Asatbek Xalimojnov 4/25/22 4:41 PM

import uz.pdp.clients.dto.UserDto;

public interface UserService {


    UserDto getUserById(Long id);
}
