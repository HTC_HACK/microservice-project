package uz.pdp.userservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.userservice.model.User;

public interface UserRepo extends JpaRepository<User,Long> {
}
