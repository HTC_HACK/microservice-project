package uz.pdp.userservice.controller;


//Asatbek Xalimojnov 4/25/22 4:40 PM


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.clients.dto.UserDto;
import uz.pdp.userservice.service.UserServiceImpl;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserServiceImpl userService;

    @GetMapping("/{userId}")
    UserDto getUserById(@PathVariable("userId") Long userId)
    {
        return userService.getUserById(userId);
    }
}
