package uz.pdp.bookreviewservice.repo;

//Asatbek Xalimojnov 4/25/22 3:05 PM

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bookreviewservice.model.Comment;
import uz.pdp.clients.projection.CommentProjection;

import java.util.List;

public interface CommentRepo extends JpaRepository<Comment,Long> {


    @Query(nativeQuery = true,value = "select id, description,created_at as creationDate, user_id as userid\n" +
            "from comments\n" +
            "where book_id =?1")
    List<CommentProjection> getAllCommentsByBookId(Long bookId);
}
