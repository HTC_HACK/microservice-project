package uz.pdp.bookreviewservice.service;

import uz.pdp.clients.projection.CommentProjection;

import java.util.List;

public interface CommentService {

    List<CommentProjection> getAllCommentsWithUsersByBookId(Long bookId);

}
