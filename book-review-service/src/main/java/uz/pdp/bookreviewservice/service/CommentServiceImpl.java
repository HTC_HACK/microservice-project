package uz.pdp.bookreviewservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.bookreviewservice.repo.CommentRepo;
import uz.pdp.clients.projection.CommentProjection;

import java.util.List;


//Asatbek Xalimojnov 4/25/22 3:07 PM

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepo commentRepo;

    @Override
    public List<CommentProjection> getAllCommentsWithUsersByBookId(Long bookId) {
        return commentRepo.getAllCommentsByBookId(bookId);
    }

}
