package uz.pdp.bookreviewservice.controller;


//Asatbek Xalimojnov 4/25/22 3:09 PM


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.bookreviewservice.service.CommentService;
import uz.pdp.clients.projection.CommentProjection;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/comment")
public class CommentController {


    private final CommentService commentService;

    @GetMapping("/with-users/{bookId}")
    List<CommentProjection> getAllCommentsWithUsersByBookId(@PathVariable Long bookId)
    {
        return commentService.getAllCommentsWithUsersByBookId(bookId);
    }



}
