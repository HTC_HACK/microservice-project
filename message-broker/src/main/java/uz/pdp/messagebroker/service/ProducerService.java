package uz.pdp.messagebroker.service;


//Asatbek Xalimojnov 4/23/22 3:53 PM


import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import uz.pdp.clients.dto.UserDto;

@Service
@RequiredArgsConstructor
public class ProducerService {

    @Value("${spring.rabbitmq.exchange}")
    String myExchange;
    @Value("${spring.rabbitmq.routing-key}")
    String myRoutingKey;

    private final RabbitTemplate rabbitTemplate;

    public String sendMessage(UserDto customerDto) {

        rabbitTemplate.convertAndSend(myExchange, myRoutingKey, customerDto);
        return "success";
    }
}
