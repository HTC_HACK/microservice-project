package uz.pdp.messagebroker.controller;


//Asatbek Xalimojnov 4/23/22 3:44 PM


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.clients.dto.UserDto;
import uz.pdp.messagebroker.service.ProducerService;

@RestController
@RequestMapping("/api/producer")
@RequiredArgsConstructor
public class ProducerController {

    private final ProducerService producerService;

    @PostMapping
    public String publish(@RequestBody UserDto customerDto) {

        return producerService.sendMessage(customerDto);
    }


}
